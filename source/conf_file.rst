Arquivos de Configuração
==================================================

`Documento para configurar a aplicação backend(API) para conexão com banco e API de Integração`

Os seguintes pontos devem ser verificados:

* This is a bulleted list.
* It has two items, the second
  item uses two lines.


#. Usuário da aplicação: verificar se existe usuário (conn_padronizacaolaudos por exemplo) e Se o mesmo tem  permissões de
:insert, update, select e delete: nas tabelas da aplicação

#. Possui o owner que será adicionado no :.env: em que o usuário da aplicação têm acesso (RL_PADRONIZACAOLAUDOS por exemplo)

#. Verificar qual o padrão de conexão utilizado pela aplicação em produção, abaixo estão alguns exemplos de conexão.

.. _my-reference-label:

Padrões de conexão
--------------------------

**PADRÃO DE CONEXÃO I:**
*utilizando todas as variáveis*::

    DB_ENGINE=oracle
    DB_NAME=hapvdes
    DB_USER=conn_padronizacaolaudos
    DB_PASSWORD=******
    DB_HOST=10.1.23.75
    DB_PORT=1521
    DB_SCHEMA=



**PADRÃO DE CONEXÃO II:**
*utilizando o DB_NAME para informar o DB_HOST e DB_PORT*::

    DB_ENGINE=oracle
    DB_NAME=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=10.1.23.75)(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=hapvdes)))
    DB_USER=conn_padronizacaolaudos
    DB_PASSWORD=******
    DB_HOST=
    DB_PORT=
    DB_SCHEMA=



*Essas configurações devem ser informados no .env da aplicação, localizado em*::

   /projeto-laudos/project  ---> no caso do contexto clínico

Ainda dentro do *.env* adicionar a informação do owner utilizado pela aplicação

# Database owner
DB_SCHEMA=WS_LAUDO


Ambiente
--------------------------

Variáveis para o proxy, secret_key  e arquivos estáticos::

    STATIC_ROOT: caminho no servidor para onde os arquivos estáticos serão copiados.
    STATIC_PATH:  caminho (no browser) onde será possível acessar o caminho no browser,  /static por default.
    PROXY_SCRIPT_NAME: Definição de root do proxy.
    SECRET_KEY: palavra passe do servidor utilizado para gerar senhas


Laudo Application Settings exemplo::

    DEBUG=True
    SECRET_KEY="7ci00$0m9ygz5yf-h67tg7u_i@3v*_ci71+a9+8m7)mm5j%xq$"
    STATIC_ROOT=/home/ubuntu/laudos/static
    STATIC_PATH=/laudos/static
    PROXY_SCRIPT_NAME=/api/laudos/protmed




Variáveis para conexão com a API de integração::

        API_URL: Informa a URL root da integração (0.0.0.0:PORTA,  app.endereco.com.br, etc)

Caso utilizar o IP e PORTA (Laudo Integration Application Settings)::

    API_BASE_URL=http://app-desenv.hapvida.com.br/api/hospital/apipadronizacaolaudos


**Após efetuar alterações no .env é necessário dar restart no apache**

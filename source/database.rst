Banco de dados
==================================================

`Documento para configurar a aplicação backend(API) para conexão com banco e API de Integração`

Os seguintes pontos devem ser verificados:

* This is a bulleted list.
* It has two items, the second
  item uses two lines.



#.Os scripts de banco de dados da aplicação se encontram no diretório: **padronizacao-laudos/banco-de-dados/v1.0.0**

    * Para os ambientes de homologação ou produção, tais scripts deverão se encontrar no CVS do Hapvida.

#.A execução dos scripts deve ser realizada na seguinte ordem:

**Tabelas criadas na aplicação**
    #. tb_django_content_type.sql
    #. tb_django_session.sql
    #. tb_auth_user.sql
    #. tb_auth_group.sql
    #. tb_auth_permission.sql
    #. tb_auth_group_permissions.sql
    #. tb_auth_user_groups.sql
    #. tb_auth_user_user_permissions.sql
    #. tb_django_admin_log.sql
    #. tb_modelo_laudo.sql
    #. tb_modelo_bloco.sql
    #. tb_modelo_variavel.sql
    #. tb_modelo_entrada.sql
    #. tb_modelo_opcao_entrada.sql
    #. tb_atendimento_laudo.sql
    #. tb_laudo.sql
    #. tb_bloco.sql
    #. tb_variavel.sql
    #. tb_entrada.sql
    #. tb_opcao_entrada.sql

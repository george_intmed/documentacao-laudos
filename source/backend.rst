Backend
==================================================

`Configurações necessárias para os servidores de desenvolvimento, homologação e produção`

Instalação de pacotes necessários
--------------------------

 #.Instalar Python 3.6 no Ubuntu::

        * cd /tmp/
        * sudo apt update
        * sudo apt upgrade
        * sudo apt install build-essential
        * sudo apt install unzip
        * sudo apt install libssl-dev zlib1g-dev libncurses5-dev libncursesw5-dev libreadline-dev libsqlite3-dev
        * sudo apt install libaio1 libaio-dev libgdbm-dev libdb5.3-dev libbz2-dev libexpat1-dev liblzma-dev tk-dev
        * wget https://www.python.org/ftp/python/3.6.0/Python-3.6.0.tar.xz
        * tar xJf Python-3.6.0.tar.xz
        * cd Python-3.6.0
        * ./configure --enable-shared LDFLAGS="-Wl,--rpath=/usr/local/lib"
        * make
        * sudo make install
        * if [ -f /usr/local/bin/pip3 ]; then exit 1; else sudo cp /usr/local/bin/pip3.6 /usr/local/bin/pip3 ; fi
        * sudo ln -s /usr/local/bin/pip3 /usr/bin/pip3

 #.Instalar mod_wsgi no Apache::

        * cd /tmp/
        * sudo apt install apache2 apache2-utils libexpat1 ssl-cert
        * sudo apt-get install apache2-dev
        * wget https://github.com/GrahamDumpleton/mod_wsgi/archive/4.6.4.tar.gz -O mod_wsgi-4.6.4.tar.gz
        * tar xvfz mod_wsgi-4.6.4.tar.gz
        * cd mod_wsgi-4.6.4
        * ./configure LDFLAGS="-Wl,--rpath -Wl,/usr/local/lib" --with-python=/usr/local/bin/python3
        * make
        * sudo make install


 #.Ativar mod_rewrite::

        * a2enmod rewrite

 #.Reinicie o Apache::

        * sudo apachectl restart

 #.Ativar mod_wsgi::

        * sudo vim /etc/apache2/conf-available/wsgi.conf

*(OBS: necessário ter o vim instalado, caso contrário executar: sudo apt-get install vim antes de rodar o comando acima.)*

 #.Adicionar a linha::

        * LoadModule wsgi_module /usr/lib/apache2/modules/mod_wsgi.so

 #.Reinicie o Apache::

        * sudo apachectl restart

 #.Instalar Oracle Instant Client + SDK (Fazer download da versão 12.2.0.1)::

        * sudo apt-get install alien


FAZER DOWNLOAD DOS ARQUIVOS DE INSTALAÇÃO
--------------------------

**Caminho no FTP IntMed:**::

    /home/hapvida/utils/oracle-instantclient12.2-basic-12.2.0.1.0-1.x86_64.rpm
    /home/hapvida/utils/oracle-instantclient12.2-devel-12.2.0.1.0-1.x86_64.rpm
    cd /tmp
    sudo alien -i oracle-instantclient12.2-basic-12.2.0.1.0-1.x86_64.rpm
    sudo alien -i oracle-instantclient12.2-devel-12.2.0.1.0-1.x86_64.rpm

**Adicione as variáveis de ambiente no final do arquivo ~/.bashrc**::

    export LD_LIBRARY_PATH=/usr/lib/oracle/12.2/client64/lib:$LD_LIBRARY_PATH
    export ORACLE_HOME=/usr/lib/oracle/12.2/client64
    export PATH=/usr/lib/oracle/12.2/client64/bin:$PATH
    source .bashrc

**Editar arquivo oracle.conf e atualizar o path da instalação do oracle**::

        sudo vim /etc/ld.so.conf.d/oracle.conf

**Editar com a seguinte linha: “/usr/lib/oracle/12.2/client64/lib”**::

        sudo ldconfig

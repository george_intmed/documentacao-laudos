Apache
==================================================

Configuração da Aplicação Django/WSGI no Apache
--------------------------

**Requisitos:**

    Versão do Apache 2.4+
    mod_wsgi com versão 4.6+

Crie um VirtualHost para hospedar a aplicação. Customize os paths caso haja necessidade. Edite o arquivo /etc/apache2/apache2.conf e adicione as seguintes linhas::

    <VirtualHost *:80>
      ServerName 10.1.22.238

      Alias /padronizacao-laudos/static /padronizacao-laudos/padronizacao-laudos-static
      <Directory /padronizacao-laudos/padronizacao-laudos-static>
        Require all granted
      </Directory>

      WSGIPassAuthorization On

      WSGIDaemonProcess padronizacao-laudos user=www-data group=root python-home=/padronizacao-laudos/api/env python-path=/padronizacao-laudos/api/project

      WSGIScriptAlias /laudos-api /padronizacao-laudos/api/project/config/wsgi.py process-group=laudos--api

      <Location /batimentos-api>
        Require all granted
        WSGIProcessGroup laudos-api
      </Location>

      ErrorLog ${APACHE_LOG_DIR}/error.log
      CustomLog ${APACHE_LOG_DIR}/access.log combined
    </VirtualHost>

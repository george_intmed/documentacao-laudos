Padronização de Laudos - Documentação oficial
==================================================

Projeto criado para padronização de laudos de forma efetiva

Overview
-----------

.. toctree::
   :maxdepth: 2

   conf_file
   database
   apache
   backend
   contents
